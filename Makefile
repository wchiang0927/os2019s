.PHONY: all directories run clean

CC=g++
CFLAGS=-std=c++11 -Wall -pthread
BIN=bin
INC=inc
OBJ=obj
SRC=src

all: directories bin/model

$(BIN)/model: $(OBJ)/model.o $(OBJ)/job.o $(OBJ)/processor.o $(OBJ)/scheduler.o $(OBJ)/task.o $(OBJ)/timer.o
	$(CC) $(CFLAGS) $^ -o $@ -I $(INC)

$(OBJ)/model.o: $(SRC)/model.cpp $(INC)/job.h $(INC)/processor.h $(INC)/scheduler.h $(INC)/task.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(OBJ)/job.o: $(SRC)/job.cpp $(INC)/job.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(OBJ)/processor.o: $(SRC)/processor.cpp $(INC)/processor.h $(INC)/job.h $(INC)/scheduler.h $(INC)/timer.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(OBJ)/scheduler.o: $(SRC)/scheduler.cpp $(INC)/scheduler.h $(INC)/job.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(OBJ)/task.o: $(SRC)/task.cpp $(INC)/task.h $(INC)/job.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(OBJ)/timer.o: $(SRC)/timer.cpp $(INC)/timer.h $(INC)/job.h $(INC)/processor.h $(INC)/scheduler.h
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

directories:
	mkdir -p $(OBJ) $(BIN)
	
run: all
	bin/model

clean:
	rm -rf $(OBJ) $(BIN)
