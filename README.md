# Info
## This is the ```Task Scheduler``` project of Team WBRB
## Team members (ordered by student ID No.)
* 105820001 顏翊純
* 105820009 汪秉沄
* 105820032 江威逸
* 105820037 蘇哲揚

# Requirements
## Operating System
```
Linux
```
## Compiler
```
g++
```

# How to run
## Run ```make``` to build the project.
## Run ```make run``` to build and run.
## Run ```make clean``` to clean the project folder.
