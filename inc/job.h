#ifndef _JOB_H_
#define _JOB_H_

#include <string>

class Job {
public :
    Job(std::string id, int priority, int timeInterval);
    bool isTerminate() const;
    std::string getID() const;
    int getPriority() const;
    int getTimeInterval() const;
    void start();
    void run();
    void interrupt();
    void terminate();

private:
    bool _isRun;
    bool _isInterrupt;
    bool _isTerminate;
    std::string _id;
    int _priority;
    int _timeInterval;
    int _count;
};

#endif
