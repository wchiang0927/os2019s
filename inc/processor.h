#ifndef _PROCESSOR_H_
#define _PROCESSOR_H_

#include "job.h"
#include "scheduler.h"

class Processor {
public :
    Processor(Scheduler * scheduler);
    void run();
    void timeIsUp();

private:
    Scheduler * _scheduler;
    Job * _currentJob;
};

#endif
