#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include <vector>

#include "job.h"

class Scheduler {
public :
    Scheduler();
    void setReadyNumber();
    void addJob(Job * job);
    Job * getNextJob();
    void popJob();

private:
    std::vector<Job *> _jobQueue;
    int _readyNumber;
};

#endif
