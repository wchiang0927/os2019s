#ifndef _TASK_H_
#define _TASK_H_

#include <vector>

#include "job.h"

class Task {
public:
    Task();
    const std::vector<Job *> & getJobs() const;

private:
    std::vector<Job *> _jobs;
};

#endif
