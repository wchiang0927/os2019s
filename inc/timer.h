#ifndef _TIMER_H_
#define _TIMER_H_

#include "processor.h"

class Timer {
public:
    Timer(Processor * processor);
    void start();

private:
    Processor * _processor;
};

#endif
