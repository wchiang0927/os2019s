#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "job.h"

Job::Job(std::string id, int priority, int timeInterval) {
    _isRun = false;
    _isInterrupt = false;
    _isTerminate = false;
    _id = id;
    _priority = priority;
    _timeInterval = timeInterval;
    _count = 0;
}

bool Job::isTerminate() const {
    return _isTerminate;
}

std::string Job::getID() const {
    return _id;
}

int Job::getPriority() const {
    return _priority;
}

int Job::getTimeInterval() const {
    return _timeInterval;
}

void Job::start() {
    if (!_isInterrupt) {
        std::cout << _id << "\033[36m" << " *** START     ***" << "\033[0m" << std::endl;
    } else {
        _isInterrupt = false;
        std::cout << _id << "\033[33m" << " *** RESUME    ***" << "\033[0m" << std::endl;
    }

    _isRun = true;
    run();
}

void Job::run() {
    while (_isRun && _count < _timeInterval) {
        _count++;
        std::cout << _id << "\033[32m" << " *** RUNNING   ***" << "\033[0m" << std::endl;

        if (_count == _timeInterval) {
            terminate();
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void Job::interrupt() {
    _isInterrupt = true;
    _isRun = false;
    std::cout << _id << "\033[35m" << " *** INTERRUPT ***" << "\033[0m" << std::endl;
}

void Job::terminate() {
    _isTerminate = true;
    _isRun = false;
    std::cout << _id << "\033[34m" << " *** TERMINATE ***" << "\033[0m" << std::endl;
}
