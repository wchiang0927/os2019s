#include <vector>

#include "job.h"
#include "processor.h"
#include "scheduler.h"
#include "task.h"

int main(int argc, char * argv[]) {
    Task task;
    Scheduler scheduler;
    Processor processor(&scheduler);
    const std::vector<Job *> jobs = task.getJobs();

    for(Job * job: jobs)  {
        scheduler.addJob(job);
    }

    processor.run();

    return 0;
}
