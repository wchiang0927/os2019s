#include <thread>

#include "processor.h"
#include "scheduler.h"
#include "timer.h"

Processor::Processor(Scheduler * scheduler) {
    _scheduler = scheduler;
}

void Processor::run() {
    Timer timer(this);
    std::thread timerThread(&Timer::start, &timer);

    _currentJob = _scheduler->getNextJob();
    _currentJob->start();
    timerThread.join();
}

void Processor::timeIsUp() {
    if (_currentJob->isTerminate()) {
        _scheduler->popJob();
    }

    _currentJob->interrupt();
    run();
}
