#include <algorithm>

#include "job.h"
#include "scheduler.h"

Scheduler::Scheduler() {
    _readyNumber = -1;
}

void Scheduler::setReadyNumber() {
    if (_readyNumber ==  (int)_jobQueue.size() - 1) {
        _readyNumber = 0;
    } else {
        _readyNumber++;
    }
}

void Scheduler::addJob(Job * job) {
    _jobQueue.push_back(job);
    std::sort(_jobQueue.begin(), _jobQueue.end(),
        [](Job * a, Job * b) {
            return a->getPriority() > b->getPriority();
        }
    );
}

Job * Scheduler::getNextJob() {
    if (_jobQueue.size() == 0) {
        exit(0);
    }

    setReadyNumber();
    return _jobQueue[_readyNumber];
}

void Scheduler::popJob() {
    delete _jobQueue[_readyNumber];
    _jobQueue.erase(_jobQueue.begin() + _readyNumber);
    _readyNumber--;
}
