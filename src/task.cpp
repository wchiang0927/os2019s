#include <vector>

#include "job.h"
#include "task.h"

Task::Task() {
    _jobs.push_back(new Job("JobA", 3, 25));
    _jobs.push_back(new Job("JobB", 5, 20));
    _jobs.push_back(new Job("JobC", 1, 15));
    _jobs.push_back(new Job("JobD", 2, 10));
    _jobs.push_back(new Job("JobE", 4, 5));
}

const std::vector<Job *> & Task::getJobs() const {
    return _jobs;
}
