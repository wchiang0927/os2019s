#include <chrono>
#include <thread>

#include "processor.h"
#include "timer.h"

Timer::Timer(Processor * processor) {
    _processor = processor;
}

void Timer::start() {
    std::this_thread::sleep_for(std::chrono::seconds(5));
    _processor->timeIsUp();
}
